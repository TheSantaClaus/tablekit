//
//  DXTKCollectionViewDataSource.h
//  DXTableKit
//
//  Created by Vladimir Shevchenko on 5/8/14.
//  Copyright (c) 2014 111min. All rights reserved.
//

#import "DXTKBaseDataSource.h"


@interface DXTKCollectionViewDataSource : DXTKBaseDataSource <UICollectionViewDataSource, UICollectionViewDelegate>

@end
