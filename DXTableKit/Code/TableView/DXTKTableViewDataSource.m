//
//  HITableViewDataSource.m
//  HomeImprovements
//
//  Created by zen on 2/21/13.
//  Copyright (c) 2013 111Minutes. All rights reserved.
//

#import "DXTKTableViewDataSource.h"
#import "DXTKContentProvider.h"

@interface DXTKTableViewDataSource ()

@end

@implementation DXTKTableViewDataSource

- (void)setRowsEditingPlugin:(id<DXTKTableViewRowsEditingPlugin>)plugin
{
    Protocol *protocol = @protocol(DXTKTableViewRowsEditingPlugin);
    if ([plugin conformsToProtocol:protocol]) {
        _rowsEditingPlugin = plugin;
    } else {
        NSAssert(NO, @"%@ should conform to %@ protocol", plugin, protocol);
    }
}

#pragma mark -
#pragma mark - UITAbleViewDataSource protocol implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.contentProvider numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentProvider numberOfItemsInSection:section];
}

- (id)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self buildCellForIndexPath:indexPath];
}

- (id<DXTKCell>)buildCellForDomainObject:(id)domainObject indexPath:(NSIndexPath*)indexPath
{
    return [self.contentView dequeueReusableCellWithIdentifier:NSStringFromClass([domainObject class])
                                                  forIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self selectCellAtIndexPath:indexPath];
}

#pragma mark -
#pragma mark -
#pragma mark -

- (UITableView *)tableView
{
    return (UITableView *)self.contentView;
}

#pragma mark -
#pragma mark - (Editing Table Rows) UItableViewDelegate protocol implementation
#pragma mark -

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    return [self.rowsEditingPlugin tableView:tableView editingStyleForRowAtIndexPath:indexPath object:object];
}

- (NSString *)tableView:(UITableView *)tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    return [self.rowsEditingPlugin tableView:tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:indexPath
                                      object:object];
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    return [self.rowsEditingPlugin tableView:tableView shouldIndentWhileEditingRowAtIndexPath:indexPath object:object];
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    [self.rowsEditingPlugin tableView:tableView willBeginEditingRowAtIndexPath:indexPath object:object];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    [self.rowsEditingPlugin tableView:tableView didEndEditingRowAtIndexPath:indexPath object:object];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.contentProvider itemForIndexPath:indexPath];
    [self.rowsEditingPlugin tableView:tableView
                   commitEditingStyle:editingStyle
                    forRowAtIndexPath:indexPath
                               object:object];
}

#pragma mark - 
#pragma mark - DXTKContentProviderDelegate protocol implementation
#pragma mark -

- (void)contentProviderWillBeginUpdates:(id<DXTKContentProvider>)contentProvider
{
    [self.tableView beginUpdates];
}

- (void)contentProviderDidEndUpdates:(id<DXTKContentProvider>)contentProvider
{
    [self.tableView endUpdates];
}

- (void)contentProvider:(id<DXTKContentProvider>)contentProvider didInsertSection:(NSUInteger)section
{
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
    [self.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
}

- (void)contentProvider:(id<DXTKContentProvider>)contentProvider didRemoveSection:(NSUInteger)section
{
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
    [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
}

- (void)contentProvider:(id<DXTKContentProvider>)contentProvider didInsertCellAtIndexPath:(NSIndexPath*)indexPath
{
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)contentProvider:(id<DXTKContentProvider>)contentProvider
 didMoveCellAtIntexPath:(NSIndexPath *)indexPath
            toIndexPath:(NSIndexPath *)newIndexPath
{
    [self.tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
}

- (void)contentProvider:(id<DXTKContentProvider>)contentProvider didUpdateCellAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
- (void)contentProvider:(id<DXTKContentProvider>)contentProvider didRemoveCellAtIndexPath:(NSIndexPath*)indexPath
{
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}


@end
